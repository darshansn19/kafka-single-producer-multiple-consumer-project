package org.training.emailservice.config;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class EmailConsumer {

	@KafkaListener(topics = "order-service", groupId = "email")
	public void consume(ConsumerRecord<String, Object> records) {
		log.info("messaage receied at email-service" + records.value() + "");

	}
}
