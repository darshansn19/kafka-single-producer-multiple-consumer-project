package org.training.stockservice.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ConsumerStockService {
	
	@KafkaListener(topics = "order-service", groupId = "stock")
	public void consume(ConsumerRecord<String, Object> records) {
		
		log.info("messaage receied at stcok-service"+records.value()+"");
		
	}


}
