package org.training.smsservice.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

//import com.twilio.Twilio;
//import com.twilio.rest.api.v2010.account.Message;
//import com.twilio.type.PhoneNumber;


@Service
@Slf4j
public class SmsConsumer {
	
	@KafkaListener(topics = "order-service", groupId = "sms")
	public void consume(ConsumerRecord<String, Object> records) {
		
		log.info("messaage receied at sms-service"+records.value()+"");
		
//		Twilio.init(System.getenv("TWILIO_ACCOUNT_SID"), System.getenv("TWILIO_AUTH_TOKEN"));
//		Message.creator(new PhoneNumber("+91"+"9741371155"), new PhoneNumber("+12185204735"),
//				"Your profile has been created successfully at ABC Bank with email " + email).create();
	

		
	}

}
