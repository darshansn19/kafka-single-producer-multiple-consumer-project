package org.training.orderservice.service;


import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.training.orderservice.dto.OrderEvent;
import org.training.orderservice.serialize.JsonSerializer;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class OrderProduceService {
	
//	private KafkaTemplate<String, OrderEvent> kafkaTemplate;

	public void sendMessage(OrderEvent event) {

		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		Producer<String, OrderEvent> kafkaProducer = new KafkaProducer<>(props, new StringSerializer(),
				new JsonSerializer<>());	

		log.info(String.format("Order event sent to kafka => %s", event.toString()));
		ProducerRecord<String, OrderEvent> records = new 
				ProducerRecord<String, OrderEvent>("order-service", event);
		log.info("" + records);
		kafkaProducer.send(records);

	}
}
