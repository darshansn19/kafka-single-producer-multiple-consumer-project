package org.training.orderservice.controller;

import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.training.orderservice.dto.Order;
import org.training.orderservice.dto.OrderEvent;
import org.training.orderservice.service.OrderProduceService;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1")
public class OrderController {
	
	private OrderProduceService orderProduceService;
	
	@PostMapping("/orders")
    public String orderPublish(@RequestBody Order order){
		
		order.setOrderId(UUID.randomUUID().toString());	
		OrderEvent orderEvent=new OrderEvent();
		orderEvent.setStatus("PENDING");
        orderEvent.setMessage("order status is in pending state");
        orderEvent.setOrder(order);

        orderProduceService.sendMessage(orderEvent);

        return "Order placed successfully ...";
    }
}
